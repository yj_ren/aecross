CrossFS: A Cross-layered Direct-Access File System
=================================================================

#### Our code release will be available in Rutgers CS SysLab GitHub page: https://github.com/RutgersCSSystems/CrossFS in a few days! :)
  
<br />
<br />
<br />

## Steps to compile and run benchmarks and applications

CrossFS is a direct-access cross-layered file system with support for concurrency. CrossFS comprises of a user-level component (libfs) and device-level component emulated using kernel (crfs) with the OS component spread across crfs and some parts of the the VFS.

The user-level component intercepts I/O calls, converts them to device-specifc nvme-based commands (VFIO), and sends request and response through shared DMA-able memory regions. The user-level component also creates per-file FD-queues to dispatch requests across queues. 

The firmware-level component (fs/crfs) provides a firmware-level file system that processes the request in parallel when possible. The firmware file system (ffs) also implements a scheduler to prioritize blocking operations.

CrossFS has several components and depdent libraries (see the code structure below). While we have tried our best to discuss the steps to install and run the code in the limited time, we believe more structuring and user-friendly directions and scripts are required, which we will do for the final code release before the conference.

### CrossFS directory structure

    ├── libfs                          # userspace library
    ├── libfs/scripts                  # scripts to mount CrossFS and microbenchmark scripts
    ├── libfs/benchmark                # microbenchmark executables
    ├── kernel/linux-4.8.12            # Linux kernel
    ├── kernel/linux-4.8.12/fs/crfs    # emulated device firmware file system (FFS)
    ├── appbench                       # application workloads
    ├── LICENSE
    └── README.md

The defslibio.c is the heart of the LibFS responsible for interacting with the FirmwareFS, create file descriptor-based I/O queues, and interval tree operations.
In the FirmwareFS (kernel/linux-4.8.12/fs/crfs), the crfs.c is responsible for fetching the request from I/O queues and processing them. Other files in crfs include file system management as well as for scalability (crfs_scalability.c) and scheduling (crfs_scheduler.c). Other files are for block, metadata, superblock, journaling managemen, and security similar to other file systems.

### CrossFS Hardware and OS Environment

CrossFS can be run on both emulated NVM and real Optane-based NVM platform by reserving a region of physical memory (similar to DAX), mounting, and use the region for storing filesystem data. 

To enable AE users to use generally available machine, this documentation will mainly focus on emulated (DAX-based) NVM platform. Users can create a cloudlab instance to run our code (see details below). Unfortunately, the NVM optane (DC memory) used in this study are loaned and requires institutional permissions and with specific OS-version. We will remove the OS version limitations during the final code release.

We currently support Ubuntu-based 16.04 kernels and all pacakge installation scripts use debian. While our changes would also run in 18.04 based Ubuntu kernel, due recent change in one of packages (Shim), we can no longer confirm this. Please see Shim discussion below.

#### Getting and Using Ubuntu 16.04 kernel
We encourage users to use NSF CloudLab (see for details). Use the image type "UWMadison744-F18". If you have questions on CloudLab, please let us know.


#### CloudLab - Partitioning a SSD and downloading the code.
If you are using CrossFS in CloudLab, first setup the CloudLab node with SSD 
and install all the required libraries.

We suggest using an Ubuntu 16.04 kernel in the CloudLab. You can use the following profile 
to create a CloudLab node "UWMadison744-F18"

Download https://gitlab.com/yj_ren/aecross/-/blob/master/cloudlab.sh from this repo. If you do not 
want to use SSD, modify the script and comment out FORMAT_DISK command.
```
./cloudlab.sh
```

##### SSD Formatting

You will be prompted with the following message. Type 'n' and press enter for all other prompts
```
Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p):
....
Last sector, +sectors or +size{K,M,G,T,P} (2048-937703087, default 937703087):
....
Created a new partition 1 of type 'Linux' and of size 447.1 GiB.
```

Now, time to save the partition. When prompted, enter 'w'. Your changes will be persisted.
```
Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.
.....
```
This will be followed by a script that installs all the required libraries. Please wait patiently 
to complete. Ath the end of it, you will see a mounted SSD partition.

NOTE: If you are prompted during Ubuntu package installation, please hit enter and all the package installation to complete.

```
cd ~/ssd
git clone https://gitlab.com/yj_ren/aecross
```

#### Changing Max open files 
sudo vim /etc/security/limits.conf

```
root             soft    nofile          1000000
root             hard    nofile          1000000
$USERNAME        soft    nofile          1000000
$USERNAME        hard    nofile          1000000
```
In addition,
```
sudo sysctl -w fs.file-max=1000000
```
### Compiling CrossFS and other kernels


#### Building CrossFS
```
cd aecross
```

#### Setting environmental variables
```
source scripts/setvars.sh
```

#### Setup for emulated NVM machine in CloudLab

#### Compile kernel and install

First compile the kernel. This would compile both CrossFS firmware-level driver and DAX.

```
$BASE/scripts/compile_kernel_install.sh (Only run this when first time install kernel)
$BASE/scripts/compile_kernel.sh (Run this when kernel is already installed)
```

Now, time to update the grub reserve NVM in the emulated region.

```
sudo vim  /etc/default/grub
```

Add the following to GRUB_CMDLINE_LINUX variable in /etc/default/grub
```
GRUB_CMDLINE_LINUX="memmap=80G\$60G"
```
This will reserve contiguous 60GB memory starting from 80GB). This will ideally
use one socket (numa node) of the system. If your system has a lower memory
capacity, please change the values accordingly.

If this does not work, then try the following. This might add the memmap configuration for all kernels. If not, just manually modify the boot parameter.
```
GRUB_CMDLINE_LINUX_DEFAULT="memmap=80G\$60G"
sudo update-grub
sudo reboot
```

After the reboot, the reserved region of memory would disappear from the OS.

### Compiling Userspace Libraries

#### Build and install user-space library (LibFS):
```
cd $LIBFS
scripts/compile_libfs.sh
```

Next, time to build SHIM library derived and enahnced from Strata [SOSP '17].
This library is responsible for intercepting POSIX I/O operations and directing them to the CrossFS client 
library for converting them to CrossFS commands.

Please note that SHIM library is currently working in 16.04 and is not support 18.04 kernels due to their recent update 
(https://github.com/pmem/syscall_intercept/issues/106). We are in the process of fixing it by ourself.

```
cd $LIBFS/libshim
./makeshim.sh
cd ..
```

#### Mounting CrossFS

```
cd $BASE/libfs
source scripts/setvars.sh
scripts/mount_crossfs.sh //(Mount CrossFS with 60GB (Starting at 80GB))
```
If successful, you will find a device-level FS mounted as follows after executing the following command
```
mount
```
none on /mnt/ram type devfs (rw,relatime,...)


NOTE: This is a complex architecture with threads in host and kernel-emulated device. If something hangs, 
try running the following that releases emulated firmware threads and either kill the processes (or restarting the machine). 
We are doing more testing to improve reliability for the final release.
```
cd $LIBFS
benchmark/crfs_exit
```

### Running Microbenchmarks

#### Reader-writer Scalability Test 
To run the scalability test, use the following. The test runs concurrent readers (1-16) 
and writers (4) that concurrently update and access a shared file. (Please wait patiently 
as it takes around 2 minutes to complete).

```
cd $LIBFS
./scripts/run_scalability_crfs.sh 
```

#### Scheduler Performance Impact Test 
To test the CrossFS urgency-aware (read-prioritized) scheduler against default round-robin scheduler, 
run the following script. The output is written to $BASE/paper-results/microbench-NVMemu/parafs-noioctl/scheduler  
under read-prioritized (rp) and round-round (rr) folders.
```
cd $LIBFS
scripts/run_scheduler_crfs.sh
```
#### Fsync tests
The tests run a stream of fsync operations across multiple threads forcing immediate commit from file descriptor queue to firmware-level file system.
The output is written to the fsync folder ...microbench-NVMemu/parafs-noioctl/fsync/$threadcount/output.txt
```
cd $LIBFS
./scripts/run_fsync_crfs.sh
```

##  Running and Evaluating Applications.

### Running Redis with CrossFS

```
cd $BASE
source scripts/setvars.sh
```

#### Compile and Setup Redis

First compile the redis-3.0.0
```
cd $BASE/appbench/redis-3.0.0  
./build_redis.sh
```
Copy redis.conf file  
```
 sudo cp redis.conf /etc/  
```

#### Run Redis benchmark 
Runs one instance of redis and redis-benchmark. The output is written to 
$BASE/paper-results/apps-NVMemu/parafs-noioctl/redis/NUM-inst with values for each instance count.
Note that after the benchmark is finished, the redis server is killed to terminate the redis server.
```
./crfs_run_small.sh
```

#### Run Redis benchmark - multiple instances (long running)
Runs for 1 to 16 redis instances.
```
./crfs_run_multi.sh
```


### Compile and Run RocksDB with CrossFS
```
cd $BASE/appbench/RocksDB
 ./build_rocksdb.sh
```

#### Run RocksDB benchmark

Run a short 4-thread RocksDB run with CrossFS
```
./crfs_run_small.sh
```

Run a full RocksDB run with CrossFS when varying the application thread count.
The output is written to $BASE/paper-results/apps-NVMemu/parafs-noioctl/rocksdb
```
./crfs_run.sh
```

### To enable NVM-based FD-queues
To enable NVM-based FD-queues, we use a in-home NVM logging library that performs persistent allocations of the 
queues.
```
cd $LIBFS
```
In the Makefile, enable the following "FLAGS+=-D_NVMFDQ" and run 
the benchmark. We will use a memory-based filesystem to store the per-FD command queues and the buffers.

To enable this, either use a DAX device mounted to */mnt/pmemdir* or to quickly try it out, allocate some 
*tmpfs* in */mnt/pmemdir*, which will be used to allocate and map FD-queues. 
```
$NVMALLOC_HOME/scripts/setuptmpfs.sh 8192
```
The example shows using 8GB of *tmpfs* for storing NVM-based FD-queues.  You can reduce or increase the size of 
the NVM queue space. If successful, you will see /mnt/pmedir mounted as *tmpfs*
Now clean the libfs and run the benchmark
```
cd $LIBFS
scripts/compile_libfs.sh
./scripts/run_scalability_crfs.sh
```
Note: We are in the process of verifying crashes and recovery which part of the ongoing camera-ready changes, 
and is not yet well tested as pointed out by reviewers. We will have them in the final code release.


# Running all Ext4-DAX
------------------------
If you were running other file systems such as CrossFS, please restart the machine

### Compiling Ext4-DAX

```
cd aecross //the base folder
```

#### Setting environmental variables
```
source scripts/setvars.sh
```

#### Setup for emulated NVM machine in CloudLab
```
cd $BASE/scripts/compile_kernel_install.sh (Only run this when first time install kernel)
cd $BASE/scripts/compile_kernel.sh (Run this when kernel is already installed)
```
Now, time to update the grub reserve NVM in the emulated region.

Add "memmap=60G!80G" to /etc/default/grub in GRUB_CMDLIN_LINUX
```
sudo update-grub
sudo reboot
```

#### Mounting Filesystem

##### Ext4-DAX
```
cd $BASE/libfs
source scripts/setvars.sh
./scripts/mountext4dax.sh
```

### Running Microbenchmarks

#### Reader-writer Scalability Test 
```
cd $BASE/libfs
./scripts/run_scalability_ext4dax.sh
```
#### Fsync tests (Figure 3)

##### Ext4-DAX/SplitFS/NOVA (FIX: Error. Shows incorrect output)
```
cd $BASE/libfs
./scripts/run_fsync_ext4dax.sh
```

##  Running and Evaluating Applications.

#### Running Redis with CrossFS
```
cd aecross
source scripts/setvars.sh
```
#### Compile and Setup Redis
First compile the redis-3.0.0
```
cd $BASE/appbench/redis-3.0.0  
make  
sudo make install  
```

Copy redis.conf file  

```
 sudo cp redis.conf /etc/  
```
#### Run Redis benchmark
```
./dax_run_multi.sh
```

### Compile RocksDB
```
 cd $BASE/appbench/RocksDB
 ./build_rocksdb.sh
```

#### Run RocksDB benchmark
```
./dax_run.sh
```

# Running all NOVA
------------------------
If you are going to run NOVA, you will need to install NOVA kernel, and reboot

### Get NOVA Github repo
```
git clone https://github.com/NVSL/linux-nova.git
cd linux-nova
```

### Checkout 4.13 kernel
```
git checkout linux-merge-v4.13
```

### Config NOVA kernel
```
cp /boot/config-$(uname -r) .config
make menuconfig
```
In the configurations to set in the step "make menuconfig", 
please see "Building NOVA" section in the NOVA Github repo: https://github.com/NVSL/linux-nova

### Compile and install NOVA kernel
```
sudo make -j32 && sudo make modules_install -j32 && sudo make install -j32
```

### Setup for emulated NVM machine in CloudLab
Add "memmap=60G!80G" to /etc/default/grub in GRUB_CMDLIN_LINUX
```
sudo update-grub
sudo reboot
```

#### Mounting NOVA Filesystem
```
cd aecross
source scripts/setvars.sh
cd $BASE/libfs
source scripts/setvars.sh
./scripts/mountnova.sh
```

### Running Microbenchmarks

#### Reader-writer Scalability Test 
```
cd $BASE/libfs
./scripts/run_scalability_nova.sh
```
#### Fsync tests (Figure 3)

##### Ext4-DAX/SplitFS/NOVA (FIX: Error. Shows incorrect output)
```
cd $BASE/libfs
./scripts/run_fsync_nova.sh
```

##  Running and Evaluating Applications.

#### Running Redis with NOVA
```
cd aecross
source scripts/setvars.sh
```
#### Compile and Setup Redis
First compile the redis-3.0.0
```
cd $BASE/appbench/redis-3.0.0  
make  
sudo make install  
```

Copy redis.conf file  

```
 sudo cp redis.conf /etc/  
```
#### Run Redis benchmark
```
./nova_run_multi.sh
```

### Compile RocksDB
```
 cd $BASE/appbench/RocksDB
 ./build_rocksdb.sh
```

#### Run RocksDB benchmark
```
./nova_run.sh
```

# Running with Strata
------------------------
We are recently fixing the library of conflict of SPDK and ndctl, and be able to run Strata on Cloudlab.
So in order to run Strata smoothly, we add kernel compiling and library build scripts along with scalability
microbenchmark in our Strata Github repo extended from the original one.

Currently Strata only runs for micro-benchmark, we are now trying filebench and others.

### Get Strata Github repo
```
git clone https://github.com/RutgersCSSystems/strata.git
cd strata
```

### Compile and install Strata's kernel
```
./scripts/compile_kernel_install.sh
```

### Setup for emulated NVM machine in CloudLab (The size is suggested in original Strata repo)
Add "memmap=16G!4G memmap=4G!20G" to /etc/default/grub in GRUB_CMDLIN_LINUX
```
sudo update-grub
sudo reboot
```

#### Compile Strata librares
```
cd strata
sudo apt-get update
./scripts/compile_strata_libs.sh
```

### Running Strata KernFS
```
cd strata
sudo ./utils/use_dax.sh bind
sudo ./utils/uio_setup.sh linux config
cd libfs
sudo ./bin/mkfs.mlfs 1
sudo ./bin/mkfs.mlfs 4
cd kernfs/tests
sudo ./run.sh kernfs
```

Then you could see KernFS is running, waiting for Input

#### Reader-writer Scalability Test 
```
cd strata
cd libfs/tests/
make
./scripts/run_scalability_strata.sh
```

The result is inside strata/libfs/tests/scalability/strata


# Running with SplitFS
------------------------

We currenlty only managed to run SplitFS for micro-benchmark

### Get SplitFS Github repo (Assume you already have an ssd partition in Cloudlab mounted with ~/ssd)
```
cd ~/ssd
git clone https://github.com/utsaslab/SplitFS.git
```

### Install dependency libraries for SplitFS
```
cd SplitFS
cd dependencies; ./kernel_deps.sh; cd ..
cd dependencies; ./splitfs_deps.sh; cd ..
```

### Compile SplitFS kernel
```
cd scripts/kernel-setup; ./compile_kernel.sh; cd ..
```

### Setup for emulated NVM machine in CloudLab (The size is suggested in original Strata repo)
Add "memmap=24G!4G nokaslr" to /etc/default/grub in GRUB_CMDLIN_LINUX
```
sudo update-grub
sudo reboot
```

#### Compile SplitFS
```
cd ~/ssd/SplitFS
cd splitfs; make clean; make; cd .. # Compile SplitFS
```

### Now switch to aecross repo and run microbench
```
cd ~/ssd/aecross/
source scripts/setvars.sh
cd libfs
source scripts/setvars.sh
./scripts/mountsplitfs.sh
./scripts/run_scalability_splitfs.sh
```


#### Final note
Final note: We are adding more documentation with smoke and unit test cases, all of which will be made available during the final code release.



















