#include "build_version.h"
const char* rocksdb_build_git_sha = "rocksdb_build_git_sha:92fa4e840c242bdfb75074790f991a5757045cc5";
const char* rocksdb_build_git_date = "rocksdb_build_git_date:2020-09-04";
const char* rocksdb_build_compile_date = __DATE__;
