#!/bin/bash

HOMEDIR=$HOME
SCRIPTS=$PWD

#Set your disk partitions
DISK=$HOME
DISK_DEVICE="/dev/sdb"
DISK_PARTITION="/dev/sdb1"

#Number of processors to use during setup
echo "Using $NPROC cores for setup"

#All downloads and code installation will happen here. 
#Feel free to change
CLOUDLABDIR=$DISK/cloudlab
#Create the CLOUDLABDIR directory
mkdir $CLOUDLABDIR

COOL_DOWN() {
	sleep 5
}

FORMAT_DISK() {
    DISK=$HOME/ssd
    mkdir $DISK
    sudo mount $DISK_PARTITION $DISK
    if [ $? -eq 0 ]; then
        sudo chown -R $USER $DISK
        echo OK
    else
        sudo fdisk $DISK_DEVICE
        sudo mkfs.ext4 $DISK_PARTITION
        sudo mount $DISK_PARTITION $DISK
        sudo chown -R $USER $DISK
    fi
}


INSTALL_CMAKE(){
    cd $CLOUDLABDIR
    wget https://cmake.org/files/v3.7/cmake-3.7.0-rc3.tar.gz
    tar zxvf cmake-3.7.0-rc3.tar.gz
    cd cmake-3.7.0*
    ./configure
    ./bootstrap
    make -j16
    make install
}

CONFIGURE_GIT() {
	git config --global user.name $USER
	git config --global user.email "youremail.com"
	git commit --amend --reset-author
}


INSTALL_SYSTEM_LIBS(){
	sudo apt-get update
	sudo apt-get install -y git
	sudo apt-get install -y kernel-package
	sudo apt-get install -y software-properties-common
	sudo apt-get install -y python3-software-properties
	sudo apt-get install -y python-software-properties
	sudo apt-get install -y unzip
	sudo apt-get install -y python-setuptools python-dev build-essential
	sudo easy_install pip
	sudo apt-get install -y numactl
	sudo apt-get install -y libsqlite3-dev
	sudo apt-get install -y libnuma-dev
	sudo apt-get install -y libkrb5-dev
	sudo apt-get install -y libsasl2-dev
	sudo apt-get install -y cmake
	sudo apt-get install -y build-essential
	sudo apt-get install -y maven
	sudo apt-get install -y mosh
	sudo apt-get install -y libmm-dev
}


FORMAT_DISK  #Create a partition.
COOL_DOWN
INSTALL_SYSTEM_LIBS
CONFIGURE_GIT
COOL_DOWN
