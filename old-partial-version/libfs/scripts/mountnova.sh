sudo mount -t NOVA -o init /dev/pmem0 /mnt/pmemdir
sudo chown -R $USER /mnt/pmemdir

MOUNT_NOVA() {
        USERN=$(whoami)
        DEVICE=/dev/pmem0
        MOUNTPOINT=/mnt/pmemdir

        echo $USERN

        if [ $(mount | grep -c $MOUNTPOINT) != 1 ]; then
                sudo umount $MOUNTPOINT
                sudo mkdir $MOUNTPOINT
                sudo mount -o dax $DEVICE $MOUNTPOINT
		sudo mount -t NOVA -o init $DEVICE $MOUNTPOINT

                sudo rm -rf $STORAGEPATH
                sudo mkdir $MOUNTPOINT/ram
                sudo ln -s $MOUNTPOINT/ram $STORAGEPATH
                sudo chown -R $USER $STORAGEPATH

                echo "$MOUNTPOINT is now mounted"
        else
                echo "$MOUNTPOINT already mounted"
        fi

        sudo chown -R $USER $MOUNTPOINT
}

MOUNT_NOVA


