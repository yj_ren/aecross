#!/bin/bash

PARAFSSCRIPT=$PWD/scripts

set -x

$PARAFSSCRIPT/run_fsync_parafs.sh
$PARAFSSCRIPT/run_scalability_parafs.sh
$PARAFSSCRIPT/run_scheduler_1readerNwriter.sh
$PARAFSSCRIPT/run_scheduler_NwriterNreader.sh
$PARAFSSCRIPT/run_scheduler_queuedepth.sh

set +x

