#!/bin/bash

# This experiment is aimed to show the impact of 
# per-file pointer queue depth


CODE=$DEVFSSRC
RESULT_BASE=$BASE/paper-results/microbench-$DEVICE/parafs-noioctl
result_dir=$RESULT_BASE/queuedepth_corecount
mkdir -p $result_dir



# Setup Parameters
let IOSIZE=4096

let READERS=-1
let WRITERS=-1
let SCHED=0
let DEVCORECOUNT=1
let QUEUEDEPTH=4

let MAX_READER=8
let MAX_DEVCORECNT=4
let MAX_QUEUEDEPTH=256

FILESIZE="2G"
FILENAME="devfile15"
FSPATH=/mnt/ram


# Create output directories
if [ ! -d "$result_dir" ]; then
	mkdir $result_dir
fi

i=1
while (( $i <= $MAX_DEVCORECNT ))
do
	dev_core="dev_core_$i"

	if [ ! -d "$result_dir/$dev_core" ]; then
		mkdir $result_dir/$dev_core/
	fi

	j=4
	while (( $j <= $MAX_QUEUEDEPTH ))
	do
		if [ ! -d "$result_dir/$dev_core/$j" ]; then
			mkdir $result_dir/$dev_core/$j
		fi

		j=$((j*4))
	done

	i=$((i*2))
done


sudo dmesg -c
cd $CODE

# Setup experiment argument list
ARGS="-q $QUEUEDEPTH -s $IOSIZE -t $READERS -u $WRITERS -p $SCHED -v $DEVCORECOUNT -b $FILESIZE"


# First fill up the test file
$CODE/benchmark/crfs_client -f "$FSPATH/$FILENAME" $ARGS


# 1 producer with different consumers with Round Robin
SCHED=0	# Round Robin
WRITERS=16
READERS=0
DEVCORECOUNT=1
# Vary the number of device CPU count
while (( $DEVCORECOUNT <= $MAX_DEVCORECNT ))
do
	dev_core="dev_core_$DEVCORECOUNT"

	QUEUEDEPTH=4
	while (( $QUEUEDEPTH <= $MAX_QUEUEDEPTH ))
	do
		ARGS="-q $QUEUEDEPTH -s $IOSIZE -t $READERS -u $WRITERS -p $SCHED -v $DEVCORECOUNT -b $FILESIZE"

		$CODE/benchmark/crfs_client -f "$FSPATH/$FILENAME" $ARGS &> $result_dir/$dev_core/$QUEUEDEPTH/output.txt
		sudo dmesg -c >> $result_dir/$dev_core/$QUEUEDEPTH/output.txt

		QUEUEDEPTH=$((QUEUEDEPTH*4))
	done

	DEVCORECOUNT=$((DEVCORECOUNT*2))
done
