#!/bin/bash

# Need to pass the number of producer as argument
default=1
producer=${1:-$default}

# Specify the base directories for code and result
CODE=$DEVFSSRC
RESULT_BASE=$DEVFSSRC/result
result_dir=$RESULT_BASE/scalability/parafs-journ


# Setup Parameters
let QSIZE=1
let NUMFS=1
let DELETEFILE=0
let IOSIZE=4096
let JOURNAL=1
let KERNIO=0

let READERS=-1
let WRITERS=-1
let SCHED=0
let DEVCORECOUNT=1
let QUEUEDEPTH=32

let MAX_READER=16
let MAX_WRITER=4

FILESIZE="12G"
FILENAME="devfile15"
FSPATH=/mnt/ram

declare -a bwarr=("4000")


THROTTLE() {
        cp $SCRIPTS/nvmemul-throttle-bw.ini $QUARTZ/nvmemul.ini
        $SCRIPTS/install_quartz.sh
}


for bw  in "${bwarr[@]}"
do
        sed -i "/ read =/c\ read =$bw" $SCRIPTS/nvmemul-throttle-bw.ini
        sed -i "/ write =/c\ write =$bw" $SCRIPTS/nvmemul-throttle-bw.ini

	result_dir=$RESULT_BASE/memthrottle/parafs-journ-$bw
        THROTTLE

	echo $result_dir

	# Create output directories
	if [ ! -d "$result_dir" ]; then
		mkdir $result_dir
	fi

	if [ ! -d "$result_dir/$producer" ]; then
		mkdir $result_dir/$producer
	fi

	# Create output directory for different number of consumers(readers)
	i=1
	while (( $i <= $MAX_READER ))
	do
		if [ ! -d "$result_dir/$producer/$i" ]; then
			mkdir $result_dir/$producer/$i
		fi

		i=$((i*2))
	done

	sudo dmesg -c
	cd $CODE

	#sudo mkdir $FSPATH
	#sudo chown -R $USER $FSPATH

	if mount | grep $FSPATH > /dev/null; then
		echo "devfs already mounted"
	else
		echo "hello"
		#$CODE/mountdevfs.sh
	fi

	# Setup experiment argument list
	ARGS="-j $JOURNAL -k $KERNIO -g $NUMFS -d $DELETEFILE -q $QUEUEDEPTH -s $IOSIZE -t $READERS -u $WRITERS -p $SCHED -v $DEVCORECOUNT -b $FILESIZE"

	# First fill up the test file
	$CODE/benchmark/crfs_client -f "$FSPATH/$FILENAME" $ARGS


	SCHED=1
	DEVCORECOUNT=4
	READERS=1
	WRITERS=1
	# Vary the number of producer(writer)
	while (( $WRITERS <= $MAX_WRITER ))
	do
		READERS=1
		# Vary the number of consumer(reader)
		while (( $READERS <= $MAX_READER ))
		do
			ARGS="-j $JOURNAL -k $KERNIO -g $NUMFS -d $DELETEFILE -q $QUEUEDEPTH -s $IOSIZE -t $READERS -u $WRITERS -p $SCHED -v $DEVCORECOUNT -b $FILESIZE"	

			$CODE/benchmark/crfs_client -f "$FSPATH/$FILENAME" $ARGS &> $result_dir/$WRITERS/$READERS/output.txt

			READERS=$((READERS*2))
		done
		WRITERS=$((WRITERS*2))
	done
   
done

